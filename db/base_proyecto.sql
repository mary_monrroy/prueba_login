-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-02-2017 a las 07:34:44
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `base_proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificaciones`
--

CREATE TABLE `calificaciones` (
  `id_usuario` int(11) NOT NULL,
  `id_competencia` int(11) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `calificacion` int(11) DEFAULT NULL,
  `usuario_creacion` varchar(20) DEFAULT NULL,
  `usuario_modificacion` varchar(20) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `id_periodo` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `calificaciones`
--

INSERT INTO `calificaciones` (`id_usuario`, `id_competencia`, `id_empleado`, `calificacion`, `usuario_creacion`, `usuario_modificacion`, `fecha_creacion`, `fecha_modificacion`, `id_periodo`) VALUES
(5, 1, 6, 1, 'wilthonp', 'wilthonp', '2017-02-11', '2017-02-11', 1),
(5, 2, 6, 1, 'wilthonp', 'wilthonp', '2017-02-11', '2017-02-11', 1),
(5, 3, 6, 1, 'wilthonp', 'wilthonp', '2017-02-11', '2017-02-11', 1),
(5, 4, 6, 1, 'wilthonp', 'wilthonp', '2017-02-11', '2017-02-11', 1),
(5, 5, 6, 1, 'wilthonp', 'wilthonp', '2017-02-11', '2017-02-11', 1),
(5, 6, 6, 1, 'wilthonp', 'wilthonp', '2017-02-11', '2017-02-11', 1),
(5, 7, 6, 1, 'wilthonp', 'wilthonp', '2017-02-11', '2017-02-11', 1),
(5, 8, 6, 1, 'wilthonp', 'wilthonp', '2017-02-11', '2017-02-11', 1),
(5, 8, 7, 1, 'wilthonp', 'wilthonp', '2017-02-18', '2017-02-18', 2),
(5, 7, 7, 1, 'wilthonp', 'wilthonp', '2017-02-18', '2017-02-18', 2),
(5, 6, 7, 1, 'wilthonp', 'wilthonp', '2017-02-18', '2017-02-18', 2),
(5, 5, 7, 1, 'wilthonp', 'wilthonp', '2017-02-18', '2017-02-18', 2),
(5, 4, 7, 1, 'wilthonp', 'wilthonp', '2017-02-18', '2017-02-18', 2),
(5, 3, 7, 1, 'wilthonp', 'wilthonp', '2017-02-18', '2017-02-18', 2),
(5, 2, 7, 1, 'wilthonp', 'wilthonp', '2017-02-18', '2017-02-18', 2),
(5, 1, 7, 1, 'wilthonp', 'wilthonp', '2017-02-18', '2017-02-18', 2),
(6, 1, 5, 4, 'cesarr', 'cesarr', '2017-02-17', '2017-02-17', 2),
(6, 2, 5, 5, 'cesarr', 'cesarr', '2017-02-17', '2017-02-17', 2),
(6, 3, 5, 5, 'cesarr', 'cesarr', '2017-02-17', '2017-02-17', 2),
(6, 4, 5, 4, 'cesarr', 'cesarr', '2017-02-17', '2017-02-17', 2),
(6, 5, 5, 4, 'cesarr', 'cesarr', '2017-02-17', '2017-02-17', 2),
(6, 6, 5, 4, 'cesarr', 'cesarr', '2017-02-17', '2017-02-17', 2),
(6, 7, 5, 5, 'cesarr', 'cesarr', '2017-02-17', '2017-02-17', 2),
(6, 8, 5, 5, 'cesarr', 'cesarr', '2017-02-17', '2017-02-17', 2),
(5, 1, 10, 1, 'wilthonp', 'wilthonp', '2017-02-17', '2017-02-17', 2),
(5, 2, 10, 1, 'wilthonp', 'wilthonp', '2017-02-17', '2017-02-17', 2),
(5, 3, 10, 1, 'wilthonp', 'wilthonp', '2017-02-17', '2017-02-17', 2),
(5, 4, 10, 1, 'wilthonp', 'wilthonp', '2017-02-17', '2017-02-17', 2),
(5, 5, 10, 1, 'wilthonp', 'wilthonp', '2017-02-17', '2017-02-17', 2),
(5, 6, 10, 1, 'wilthonp', 'wilthonp', '2017-02-17', '2017-02-17', 2),
(5, 7, 10, 1, 'wilthonp', 'wilthonp', '2017-02-17', '2017-02-17', 2),
(5, 8, 10, 1, 'wilthonp', 'wilthonp', '2017-02-17', '2017-02-17', 2),
(8, 1, 9, 1, 'bryans', 'bryans', '2017-02-18', '2017-02-18', 2),
(8, 2, 9, 1, 'bryans', 'bryans', '2017-02-18', '2017-02-18', 2),
(8, 3, 9, 1, 'bryans', 'bryans', '2017-02-18', '2017-02-18', 2),
(8, 4, 9, 1, 'bryans', 'bryans', '2017-02-18', '2017-02-18', 2),
(8, 5, 9, 1, 'bryans', 'bryans', '2017-02-18', '2017-02-18', 2),
(8, 6, 9, 1, 'bryans', 'bryans', '2017-02-18', '2017-02-18', 2),
(8, 7, 9, 1, 'bryans', 'bryans', '2017-02-18', '2017-02-18', 2),
(8, 8, 9, 1, 'bryans', 'bryans', '2017-02-18', '2017-02-18', 2),
(9, 1, 8, 1, 'luisg', 'luisg', '2017-02-18', '2017-02-18', 2),
(9, 2, 8, 1, 'luisg', 'luisg', '2017-02-18', '2017-02-18', 2),
(9, 3, 8, 1, 'luisg', 'luisg', '2017-02-18', '2017-02-18', 2),
(9, 4, 8, 1, 'luisg', 'luisg', '2017-02-18', '2017-02-18', 2),
(9, 5, 8, 1, 'luisg', 'luisg', '2017-02-18', '2017-02-18', 2),
(9, 6, 8, 1, 'luisg', 'luisg', '2017-02-18', '2017-02-18', 2),
(9, 7, 8, 1, 'luisg', 'luisg', '2017-02-18', '2017-02-18', 2),
(9, 8, 8, 1, 'luisg', 'luisg', '2017-02-18', '2017-02-18', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencias`
--

CREATE TABLE `competencias` (
  `id_competencia` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_creacion` varchar(10) DEFAULT NULL,
  `usuario_modificacion` varchar(10) DEFAULT NULL,
  `id_estado` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `competencias`
--

INSERT INTO `competencias` (`id_competencia`, `nombre`, `descripcion`, `fecha_creacion`, `fecha_modificacion`, `usuario_creacion`, `usuario_modificacion`, `id_estado`) VALUES
(1, 'Habilidades basicas', 'Expresion y capacidad de escuchar', '2016-12-09', '2016-12-09', 'bryan', 'bryan', 1),
(2, 'aptitudes analiticas', 'pensar creativamente, tomar deciciones y solucionar problemas', '2016-12-09', '2016-12-09', 'bryan', 'bryan', 1),
(3, 'cualidades personales', 'responsabilidad y autoestima', '2016-12-09', '2016-12-09', 'bryan', 'bryan', 1),
(4, 'Gestion de recursos', 'administrar y distribuir recursos', '2017-01-12', '2017-01-12', 'wilthon', 'wilthon', 1),
(5, 'Relacionesinterpersonales', 'trabajo en equipo y liderazgo', '2017-01-12', '2017-01-12', 'wilthon', 'wilthon', 1),
(6, 'Gestion de informacion', 'buscar y evaluar infrmacion', '2017-01-12', '2017-01-12', 'wilthon', 'wilthon', 1),
(7, 'Comprension sistematica', 'monirorear y seguir desempeño', '2017-01-12', '2017-01-12', 'wilthon', 'wilthon', 1),
(8, 'Dominio tecnologico', 'Seleccionar tecnologias y aplicarlas en tareas asignadas', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id_departamento` int(11) NOT NULL,
  `departamento` varchar(25) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_creacion` varchar(10) DEFAULT NULL,
  `usuario_modificacion` varchar(10) DEFAULT NULL,
  `id_estado` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id_departamento`, `departamento`, `fecha_creacion`, `fecha_modificacion`, `usuario_creacion`, `usuario_modificacion`, `id_estado`) VALUES
(1, 'sistemas', '2016-12-09', '2016-12-09', 'bryan', 'bryan', 1),
(2, 'contabilidad', '2016-12-09', '2016-12-09', 'bryan', 'bryan', 1),
(4, 'Gerencia', '2017-01-14', '2017-01-14', NULL, NULL, 1),
(3, 'Finanzas', '2017-01-13', '2017-01-13', NULL, NULL, 1),
(5, 'RRHH', '2017-01-19', '2017-01-19', 'wilthonp', 'wilthonp', 1),
(6, 'ventas', '2017-01-23', '2017-01-23', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id_empleado` int(2) NOT NULL,
  `usuario` varchar(10) NOT NULL,
  `nombres` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `fecha_nacimiento` date NOT NULL,
  `id_departamento` int(11) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_creacion` varchar(10) DEFAULT NULL,
  `usuario_modificacion` varchar(10) DEFAULT NULL,
  `id_estado` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id_empleado`, `usuario`, `nombres`, `apellidos`, `clave`, `foto`, `fecha_nacimiento`, `id_departamento`, `fecha_creacion`, `fecha_modificacion`, `usuario_creacion`, `usuario_modificacion`, `id_estado`) VALUES
(1, 'bsilva', 'bryan ', 'silva', '25f9e794323b453885f5181f1b624d0b', 'C:/wamp64/www/proyecto/imagenes/fotosEmpleados/0.png', '1995-02-19', 1, '2017-01-29', '2017-01-29', 'root', 'root', 1),
(2, 'squiroz', 'santiago ', 'quiroz', '25d55ad283aa400af464c76d713c07ad', 'C:/wamp64/www/proyecto/imagenes/fotosEmpleados/1.png', '1992-07-16', 1, '2017-01-29', '2017-01-29', 'bsilva', 'bsilva', 1),
(3, 'jlopez', 'jairo', 'lopez', '25d55ad283aa400af464c76d713c07ad', 'C:/wamp64/www/proyecto/imagenes/fotosEmpleados/2.jpg', '1993-05-20', 1, '2017-01-29', '2017-01-29', 'bsilva', 'bsilva', 1),
(4, 'squiroz', 'santiago', 'quiroz', '25d55ad283aa400af464c76d713c07ad', 'C:/wamp64/www/proyecto/imagenes/fotosEmpleados/3.png', '1995-02-07', 2, '2017-01-29', '2017-02-07', 'bsilva', 'bsilva', 1),
(5, 'mvillalta', 'madelaine', 'villalta', '25d55ad283aa400af464c76d713c07ad', 'C:/wamp64/www/proyecto/imagenes/fotosEmpleados/14.png', '1994-04-04', 1, '2017-01-29', '2017-01-29', 'bsilva', 'bsilva', 1),
(6, 'mmonroy', 'marilyn ', 'monroy', '25d55ad283aa400af464c76d713c07ad', 'C:/wamp64/www/proyecto/imagenes/fotosEmpleados/11.jpg', '1995-08-08', 1, '2017-01-29', '2017-01-29', 'bsilva', 'bsilva', 1),
(7, 'palvarez', 'paco', 'alvarez', '25d55ad283aa400af464c76d713c07ad', 'C:/wamp64/www/proyecto/imagenes/fotosEmpleados/4.png', '2017-02-07', 4, '2017-02-07', '2017-02-08', 'bsilva', 'bsilva', 1),
(8, 'jalvarez', 'jose', 'alvarez   ', '25d55ad283aa400af464c76d713c07ad', 'C:/wamp64/www/proyecto/imagenes/fotosEmpleados/15.png', '2010-10-10', 2, '2017-02-08', '2017-02-08', 'mmonroy', 'mmonroy', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id_estado` int(2) NOT NULL,
  `estado` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id_estado`, `estado`) VALUES
(1, 'activo'),
(2, 'inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fecha_calificaciones`
--

CREATE TABLE `fecha_calificaciones` (
  `id_periodo` int(2) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `usuario_creacion` varchar(50) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `id_estado` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fecha_calificaciones`
--

INSERT INTO `fecha_calificaciones` (`id_periodo`, `fecha_inicio`, `fecha_fin`, `descripcion`, `usuario_creacion`, `fecha_creacion`, `id_estado`) VALUES
(1, '2017-02-01', '2017-02-27', 'fecha 1', 'wilthonp', '2017-02-11', 2),
(2, '2017-02-02', '2017-02-28', 'fecha 2', 'wilthonp', '2017-02-11', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jefe_departamento`
--

CREATE TABLE `jefe_departamento` (
  `id_departamento` int(2) NOT NULL,
  `id_empleado` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `jefe_departamento`
--

INSERT INTO `jefe_departamento` (`id_departamento`, `id_empleado`) VALUES
(1, 5),
(2, 8);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `calificaciones`
--
ALTER TABLE `calificaciones`
  ADD PRIMARY KEY (`id_usuario`,`id_competencia`,`id_empleado`),
  ADD KEY `id_competencia` (`id_competencia`),
  ADD KEY `id_empleado` (`id_empleado`);

--
-- Indices de la tabla `competencias`
--
ALTER TABLE `competencias`
  ADD PRIMARY KEY (`id_competencia`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id_departamento`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `fecha_calificaciones`
--
ALTER TABLE `fecha_calificaciones`
  ADD PRIMARY KEY (`id_periodo`),
  ADD UNIQUE KEY `fecha_inicio` (`fecha_inicio`),
  ADD UNIQUE KEY `fecha_fin` (`fecha_fin`);

--
-- Indices de la tabla `jefe_departamento`
--
ALTER TABLE `jefe_departamento`
  ADD PRIMARY KEY (`id_departamento`),
  ADD UNIQUE KEY `id_empleado` (`id_empleado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `competencias`
--
ALTER TABLE `competencias`
  MODIFY `id_competencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id_departamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `fecha_calificaciones`
--
ALTER TABLE `fecha_calificaciones`
  MODIFY `id_periodo` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
